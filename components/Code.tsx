import React, {ComponentProps} from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';
import {solarizedDark} from 'react-syntax-highlighter/dist/cjs/styles/hljs';

export default function Code(props: { className: string, children: string }) {
    return (
        <SyntaxHighlighter language={props.className.replaceAll("language-", "")} style={solarizedDark} >
            {props.children}
        </SyntaxHighlighter>
    );
}