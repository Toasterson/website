import React from "react";
import styled from 'styled-components'

const textColor = "#cecece"

const StyledHeader = styled.header`
    height: 100px;
    background-image: url("/img/header-bg.webp");
    color: ${textColor};
    padding: 50px;
`

const width = 80;

const StyledH1 = styled.h1`
    margin: auto;
    width: ${width}%;
    padding: 10px;
    text-align: center;
`


const PBackgroundColor = "#383838";
const StyledP = styled.p`
    margin: auto;
    width: ${width}%;
    padding: 10px;
    text-align: center;
    font-weight: bold;
    background-color: ${PBackgroundColor};
`

export default function Header() {
    return <StyledHeader>
        <StyledH1>Toasty's Rambles</StyledH1>
        <StyledP>This is my personal blog, where I write about stuff I care about.
            It may be something coherent but maybe not.</StyledP>
    </StyledHeader>
}