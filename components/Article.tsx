import React from "react";
import {Post} from "../api";
import styled from 'styled-components'
import dynamic from "next/dynamic";

export interface PostProps {
    post: Post
}

const BorderColor = "#afacac"
const BackgroundColor = "#eaeaea"
const StyledArticle = styled.article`
    margin: 10px auto 20px auto;
    width: 70%;
    padding: 10px 40px 40px 40px;
    border: 1px solid ${BorderColor};
    background-color: ${BackgroundColor};
`

const StyledAnchorLink = styled.a`
    color: black;
    text-decoration: none;
    :hover {
        color: black;
        text-decoration: none;
    }
`

export default function Article({post}: PostProps) {
    const MD = dynamic(() => import(`../posts/${post.key}.mdx`));
    return <StyledArticle>
        <>
            <a id={post.slug} />
            <h1><StyledAnchorLink href={'/blog/'+post.slug}># {post.attributes.title}</StyledAnchorLink></h1>
            <MD />
        </>
    </StyledArticle>
}