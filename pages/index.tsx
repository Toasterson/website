import React from "react";
import {GetStaticPropsContext, GetStaticPropsResult} from 'next'
import {getAllPosts, Post} from "../api";
import {ParsedUrlQuery} from "querystring";
import Header from "../components/Header";
import Article from "../components/Article"
import Head from "next/head"

//@ts-ignore
import {MDXProvider} from '@mdx-js/react'
import Code from "../components/Code";

interface HomeProps {
    posts: Post[]
}

const components = {
    code: Code
}

export default function Home(props: HomeProps) {
    return (
        <>
            <Head>
                <html lang="en-us" />
                <link rel="stylesheet" href="/styles/styles.css" />
                <title>Toasty's Rambles</title>
            </Head>
            <Header />
            <MDXProvider components={components}>
                {props.posts.sort((a,b) => (
                    // Turn your strings into dates, and then subtract them
                    // to get a value that is either negative, positive, or zero.
                    // @ts-ignore
                    new Date(b.attributes.date) - new Date(a.attributes.date))
                ).map((post: Post) => <Article key={post.key} post={post} />)}
            </MDXProvider>
        </>
    )
}

// @ts-ignore
export async function getStaticProps(context: GetStaticPropsContext<ParsedUrlQuery>): Promise<GetStaticPropsResult<HomeProps>>{
    const posts = await getAllPosts()
    return {
        props: {
            posts,
        }
    }
}