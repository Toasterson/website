import React from "react";
import {getPost, Post} from "../../api";
import Header from "../../components/Header";
import Article from "../../components/Article"
import { useRouter } from 'next/router'
import dynamic from "next/dynamic";

//@ts-ignore
import {MDXProvider} from '@mdx-js/react'
import Code from "../../components/Code";
import Head from "next/head";

interface HomeProps {
    posts: Post[]
}

const components = {
    code: Code
}

interface PostProps {
    post: Post
}

export default function PostComponent({post}: PostProps) {
    const router = useRouter()
    const { slug, title } = router.query
    if (typeof slug !== "string") {
        return <>
            <Head>
                <html lang="en-us" />
                <link rel="stylesheet" href="/styles/styles.css" />
                <title>Toasty's Rambles</title>
            </Head>
            <Header />
            <div>
                No Post could be found sorry.
            </div>
        </>
    }

    return <>
            <Head>
                <html lang="en-us" />
                <link rel="stylesheet" href="/styles/styles.css" />
                <title>Toasty's Rambles</title>
            </Head>
            <Header />
            <MDXProvider components={components}>
                <Article key={slug} post={post} />)
            </MDXProvider>
        </>
        
}

// @ts-ignore
export async function getStaticProps(context: GetStaticPropsContext<ParsedUrlQuery>): Promise<GetStaticPropsResult<PostProps>>{
    const post = await getPost(context.params.slug)
    return {
        props: {
            post,
        }
    }
}

export async function getStaticPaths() {

    const paths = []
    const mdxContext = require.context('../../posts', true, /\.mdx$/)

    for (const key of mdxContext.keys()) {
        try {
            const post = key.slice(2);
            const slug = post.replace('.mdx', '');
            paths.push({
                params: {
                    slug: slug,
                }
            })
        } catch (e) {
            console.log(e)
        }
    }

    return {
      paths: paths,
      fallback: false,
    };
  }
  