import Metadata from "./metadata";

export interface Post {
    slug: string,
    key: string,
    attributes: Metadata
}

export async function getAllPosts(): Promise<Post[]> {
    const posts = []
    const mdxContext = require.context('../posts', true, /\.mdx$/)

    for (const key of mdxContext.keys()) {
        try {
            const post = key.slice(2);
            const {metadata} = await import(`../posts/${post}`);
            const slug = post.replace('.mdx', '');
            posts.push({
                key: slug,
                slug: slug,
                attributes: metadata,
            })
        } catch (e) {
            console.log(e)
        }
    }

    return posts;
}

export async function getPost(slug: string): Promise<Post> {
    const {metadata} = await import(`../posts/${slug}.mdx`);
    return {
        key: slug,
        slug: slug,
        attributes: metadata,
    }
}