export default interface Metadata {
    title: string
    draft: boolean | null
    date: string | null
}